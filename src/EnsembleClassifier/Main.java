/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnsembleClassifier;

import MyWekaUtilPackage.GlobalDataSettings;
import MyWekaUtilPackage.MyClassifierUtils;
import MyWekaUtilPackage.MyWekaUtils;

/**
 *
 * @author mohammad
 */
public class Main {

    

    public static void main(String[] args) throws Exception {
        //String projPath = "/Users/nazmul/MyGitRepo/Data/";
        String projPath = "/home/mohammad/Dataset/UCI-Datasets/DEEOC/Abalone/";
        String trnFile = projPath + "train-abalone.arff";
        String tstFile = projPath + "Test-abalone.arff";
        //String trnFile = projPath + "Train-Data-AD-5-Prot.arff";
        //String tstFile = projPath + "Test-Data-AD.arff";
        String cvModelPath = projPath + "CVModel/";
        String cvDataPath = projPath + "CVData/";
        String modelPath = projPath + "FullModel/";
        String logFile = projPath + "Log-EoC-AD-5-Eval.txt";

        //double[] allClsWeight = {0.8148335501, 0.8034809196, 0.8034809196, 0.7464417758, 0.8476387719, 0.8266448376, 0.869899067, 0.8056990521, 0.7855844048, 0.7420439079, 0.7420439079, 0.8034809196, 0.6936879756, 0.893006841, 0.7380069296, 0.8034809196, 0.7901266635, 0.8056990521, 0.8904816524, 0};
        double[] individual = {0.8148335501, 0.0, 0.8034809196, 0.0, 0.0, 0.8266448376, 0.0, 0.8056990521, 0.7855844048, 0.0, 0.0, 0.0, 0.6936879756, 0.893006841, 0.0, 0.8034809196, 0.0, 0.0, 0.8904816524, 0.0};
        //double[] normIndiv=normalizeWeight(individual);
        
        
        GlobalDataSettings mySettings = new GlobalDataSettings();
        mySettings.enableDebug();
        mySettings.setTrainFile(trnFile);
        mySettings.setCVDataPath(cvDataPath);
        mySettings.setModelPath(modelPath);

        // Load Training Data and Generate Full Models
        MyWekaUtils myDataManipulator = new MyWekaUtils();
        myDataManipulator.loadTrainData(trnFile);
        myDataManipulator.loadTestData(tstFile);

        /*MyClassifierUtils clsfB=new MyClassifierUtils();
        clsfB.GenerateFullModel(modelPath, trnFile, tstFile, logFile);
        */
        
        Ensemble anEoC = new Ensemble();
        anEoC.testModel(modelPath, individual, myDataManipulator.getTrainData(), myDataManipulator.getTestData());
        
        System.out.println("Done!");
    }

}
