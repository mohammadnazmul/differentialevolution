/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnsembleClassifier;

/**
 *
 * @author nazmul
 */
    

import java.util.ArrayList;
import java.util.List;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.core.Capabilities;
import weka.core.Instance;
import weka.core.Instances;


/**
 *
 * @author mohammad
 */
public class EOC extends AbstractClassifier {

    /**
     *
     */
//    private static final long serialVersionUID = -8996052328536347834L;
    List<Classifier> classifiers = new ArrayList<Classifier>();

    @Override
    public void buildClassifier(Instances data) throws Exception {
        for (Classifier classifier : classifiers) {
            classifier.buildClassifier(data);
        }
    }

    @Override
    public double classifyInstance(Instance instance) throws Exception {
        double sum = 0.0;
        for (Classifier classifier : classifiers) {
            double classification = classifier.classifyInstance(instance);
            sum += classification;
        }
        if (sum > classifiers.size() / 2) {
            return 1.0;
        }
        return 0.0;
    }

    /*
     * Sets the list of possible classifers to choose from.
     */
    public void setPreBuiltClassifiers(List<Classifier> arr_classifiers) {
        classifiers = arr_classifiers;
    }

    /*
     * Gets the list of possible classifers to choose from.
     */
    public List<Classifier> getClassifiers() {
        return classifiers;
    }

    public Classifier getClassifier(int index) {
        return classifiers.get(index);
    }

    /*
     * Returns combined capabilities of the base classifiers, i.e., the
     * capabilities all of them have in common.
     */
    public Capabilities getCapabilities() {
        Capabilities result;
        int i;

        result = (Capabilities) getClassifier(0).getCapabilities().clone();
        for (i = 1; i < getClassifiers().size(); i++) {
            result.and(getClassifier(i).getCapabilities());
        }

        // set dependencies
        for (Capabilities.Capability cap : Capabilities.Capability.values()) {
            result.enableDependency(cap);
        }

        result.setOwner(this);

        return result;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public void addClassifier(Classifier classifier) {
        classifiers.add(classifier);
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Ensemble of Classifier\n");
        buffer.append("-----------------\n");
        for (Classifier classifier : classifiers) {
            buffer.append(classifier.getClass().getSimpleName()+"\n");
        }
        buffer.append("\n-----------------\n");

        return buffer.toString();
    }
}

