/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnsembleClassifier;

import java.util.ArrayList;
import java.util.List;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.core.Capabilities;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class WeightedEoC extends AbstractClassifier {

    List<Classifier> classifiers = new ArrayList<Classifier>();
    List<Double> weights = new ArrayList<Double>();
    double wsum = 0.0;

    @Override
    public void buildClassifier(Instances data) throws Exception {
        for (Classifier classifier : classifiers) {
            classifier.buildClassifier(data);
        }
    }

    // The Weighted Majority Algorithm
    // Mick Littlestone, Manfred K. Warmuth
    // http://www.cs.iastate.edu/~honavar/winnow1.pdf
    @Override
    public double classifyInstance(Instance instance) throws Exception {
        double pos = 0.0, neg = 0;
        int idx = 0;

        for (Classifier classifier : classifiers) {
            double classification = classifier.classifyInstance(instance);
            if (classification > 0.0) {
                pos += weights.get(idx);
            } else {
                neg += weights.get(idx);
            }
            idx++;
        }

        if (pos > neg) {
            return 1.0;
        } else if (neg > pos) {
            return 0.0;
        } else {
            // resolve the tie
            double n = Math.random();
            if (n >= 0.5) {
                return 1.0;
            } else {
                return 0.0;
            }
        }
    }

    /*
     * Sets the list of possible classifers to choose from.
     */
    public void setPreBuiltClassifiers(List<Classifier> arr_classifiers) {
        classifiers = arr_classifiers;
    }

    public void setPreBuiltClassifiersWeights(List<Classifier> arr_classifiers, List<Double> weights) {
        classifiers = arr_classifiers;
        this.weights = weights;
    }

    /*
     * Gets the list of possible classifers to choose from.
     */
    public List<Classifier> getClassifiers() {
        return classifiers;
    }

    public Classifier getClassifier(int index) {
        return classifiers.get(index);
    }

    /*
     * Returns combined capabilities of the base classifiers, i.e., the
     * capabilities all of them have in common.
     */
    public Capabilities getCapabilities() {
        Capabilities result;
        int i;

        result = (Capabilities) getClassifier(0).getCapabilities().clone();
        for (i = 1; i < getClassifiers().size(); i++) {
            result.and(getClassifier(i).getCapabilities());
        }

        // set dependencies
        for (Capabilities.Capability cap : Capabilities.Capability.values()) {
            result.enableDependency(cap);
        }

        result.setOwner(this);

        return result;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public void addClassifier(Classifier classifier) {
        classifiers.add(classifier);
    }

    public void addClassifierWeights(Classifier classifier, double clsWeight) {
        classifiers.add(classifier);
        weights.add(clsWeight);

        wsum += clsWeight;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Ensemble of Classifier\n");
        buffer.append("----------------------\n");
        int idx = 0;
        for (Classifier classifier : classifiers) {
            String clsStr = classifier.getClass().getSimpleName() + "\t(" + weights.get(idx) + ")" + "\n";
            idx++;
            buffer.append(clsStr);
        }
        buffer.append("-----------------------\n");

        return buffer.toString();
    }
}
