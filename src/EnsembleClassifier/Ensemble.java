/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnsembleClassifier;

import static MyWekaUtilPackage.GlobalDataSettings.clsfNames;
import java.util.ArrayList;
import java.util.List;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class Ensemble {

    boolean isDebug = true;
    WeightedEoC myEOC;
    String pathCVModels;
    String pathFullModels;
    String strCmb;
    Evaluation testEval, trnCVEval;
    List<Classifier> baseClassifiers = new ArrayList<Classifier>();
    List<Double> clsfWeight = new ArrayList<Double>();

    Instances testFullData;

    double arraySum(double[] vals) {
        double sum = 0.0;
        for (int i = 0; i < vals.length; i++) {
            sum += vals[i];
        }
        return sum;
    }

     double[] normalizeWeight(double[] vals) {
        double[] normVals = new double[vals.length];
        double sum = arraySum(vals);
        
        for (int i = 0; i < vals.length; i++) {
            normVals[i] = (vals[i] / sum);
            System.out.print(normVals[i]+", ");
        }

        sum = arraySum(normVals);
        
        return normVals;
    }
     
    public double testModel(String modelPath, double[] cmb, Instances trnData, Instances tstData) throws Exception {
        pathFullModels = modelPath;
        testEval = new Evaluation(trnData);
        setEnsemble(cmb);
        System.out.println(myEOC.toString());
        //myEOC.buildClassifier(trnData);
        testEval.evaluateModel(myEOC, tstData);

        // output evaluation
        double fit = testEval.weightedMatthewsCorrelation();
        if (isDebug) {
            System.out.println();
            System.out.println(testEval.toMatrixString("=== Confusion matrix for Ensemble of Classifier on Testing Dataset ==="));
            System.out.println(testEval.toSummaryString());
            System.out.println("MCC:" + fit);
        }
        return fit;
    }

    private void setEnsemble(double[] cmb) throws Exception {
        myEOC = new WeightedEoC();
        double[] normWeight=normalizeWeight(cmb);
        
        for (int i = 0; i < cmb.length; i++) {
            if (cmb[i] > 0.0) {
                String clsfPath = pathFullModels + clsfNames[i] + ".model";

                Classifier cls = (Classifier) weka.core.SerializationHelper.read(clsfPath);
                baseClassifiers.add(cls);
                clsfWeight.add(normWeight[i]);
                if (isDebug) {
                    System.out.println("Loaded: " + clsfPath);
                }
            } // END:if 
        } //END:for cmb.length
        myEOC.setPreBuiltClassifiersWeights(baseClassifiers, clsfWeight);
    }
}
