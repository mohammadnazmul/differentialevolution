/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyDifferentialEvolution;
import EnsembleClassifier.Ensemble;

/**
 *
 * @author mohammad
 */
public class TheProblem {

    /**
     * Returns the name of the problem.
     *
     * @return The name of the problem.
     */
    
    
    public String getName() {
        return "Diferential Evolution-based Ensemble of Classifier Combination Search";
    }

    /**
     * Returns the number of dimensions of this problem.
     *
     * @return The number of dimensions of this problem.
     */
    public int getDimensionsNumber() {
        return 20;
    }

    /**
     * Returns the lower limit of the search space for this problem
     *
     * @return The lower limit of the search space for this problem
     */
    public double getLowerLimit(int dimension) {
        return 0.0;
    }

    /**
     * Returns the upper limit of the search space for this problem
     *
     * @return The upper limit of the search space for this problem
     */
    public double getUpperLimit(int dimension) {
        return 1.0;
    }

    /**
     * Compares the bestSolutionFitness with the currentSolutionFitness and
     * returns true. if currentSolutionFitness has a better fitness
     * than bestSolutionFitness, otherwise it returns false.
     */
    public boolean compareFitness(double pBestFitness, double currentPositionFitness) {
        return currentPositionFitness > pBestFitness;
    }

    /**
     * Calculates the fitness of the given solution.
     *
     * @param solution The values of the solution in each dimension of the
     * problem.
     * @return The fitness of the given solution.
     */
    public double getFitness(double... dimension) {
        double result = 0.0;
        for (int i = 0; i < dimension.length - 1; i++) {
            result += dimension[i];
        }
        return result;
        
        // I have to call EoC and Evaluate the MCC for the real-coded combination
        /*double result=0.0;
        Ensemble deEoC=new Ensemble();
                        //modelPath indiv       trnData  tstData
        //deEoC.testModel(    null,   dimension,  null,       null);
        
        return result;*/
    }
}
