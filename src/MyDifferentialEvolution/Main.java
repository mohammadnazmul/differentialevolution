/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyDifferentialEvolution;

import MyWekaUtilPackage.GlobalDataSettings;
import static MyWekaUtilPackage.GlobalDataSettings.clsfNames;
import static MyWekaUtilPackage.GlobalDataSettings.cvModels;
import MyWekaUtilPackage.MyWekaUtils;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class Main {
/*
    static String trnFile;
    //String tstFile = projPath + "Test-Data-AD.arff";
    static String cvModelPath;
    static String cvDataPath;
    //String modelPath = projPath + "FullModel/";
    static String logFile;
    static GlobalDataSettings mySettings;

    public static void loadCVModels() {
        for (int f = 1; f < 10; f++) {
            for (int c = 1; c < clsfNames.length; c++) {
                try {
                    String modelName = cvModelPath + clsfNames[c] + "-" + f + ".model";
                    Classifier cls = (Classifier) weka.core.SerializationHelper.read(modelName);
                    cvModels[f][c] = cls;
                } catch (Exception ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static void loadCVData() {
        try {
            mySettings = new GlobalDataSettings();
            mySettings.setCVDataPath(cvDataPath);
            MyWekaUtils myDataManipulator = new MyWekaUtils();
            myDataManipulator.loadAllCVData(cvDataPath);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static double[] initClassWeight() {
        double[] theWeight = new double[20];

        loadCVData();
        loadCVModels();

        Classifier cls;
        for (int c = 1; c < clsfNames.length; c++) {
            for (int f = 0; f < 10; f++) {
                cls = cvModels[f][c];
                // Test the model
                //Instances train = cvTstData[f];
                //Evaluation eTest = new Evaluation(train);
                //eTest.evaluateModel(cModel, test);
            }
        }
        return theWeight;
    }

    public static double[] initRandomWeight(int dim) {
        double[] weights = new double[dim];
        //note a single Random object is reused here
        Random randomGenerator = new Random();
        for (int i = 0; i < dim; i++) {
            weights[i]=randomGenerator.nextDouble();
        }
        return weights;
    }

    public static void main(String[] args) throws Exception {
        /*
        String projPath = "//home/mohammad/Dataset/UCI-Datasets/DEEOC/BUPA/";
        //String trnFile = projPath + "train-abalone.arff";
        //String tstFile = projPath + "Test-abalone.arff";
        trnFile = projPath + "bupa.arff";
        //String tstFile = projPath + "Test-Data-AD.arff";
        cvModelPath = projPath + "CVModel/";
        cvDataPath = projPath + "CVData/";
        //String modelPath = projPath + "FullModel/";
        String logFile = projPath + "Log-DEEoC-BUPA-Eval.txt";

        GlobalDataSettings data = new GlobalDataSettings();
        //data.setCVDataPath(cvDataPath);
        //data.setCVModelPath(cvModelPath);
        //data.setFold(10);
        //data.setTrainFile(trnFile);

        MyWekaUtils clsfUtil = new MyWekaUtils();
        clsfUtil.loadAllCVData(cvDataPath);
        clsfUtil.loadCVModels(cvModelPath);
                */

/*      TheProblem prob = new TheProblem();
        //double[] allClsWeight = {0.8148335501, 0.8034809196, 0.8034809196, 0.7464417758, 0.8476387719, 0.8266448376, 0.869899067, 0.8056990521, 0.7855844048, 0.7420439079, 0.7420439079, 0.8034809196, 0.6936879756, 0.893006841, 0.7380069296, 0.8034809196, 0.7901266635, 0.8056990521, 0.8904816524, 0};
        
        System.out.println("Initialize Random Weights...");
        double[] allClsWeight=initRandomWeight(20);
        //                                                 dim, popSize, maxGen, stDev,  scaleFact,  crossoverProb,  TheProblem ClsfWeights
        DifferentialEvolution de = new DifferentialEvolution(20, 20, 10000, 0.02, 0.5, 0.9, prob, allClsWeight);
        System.out.println("Starting Differential Evolution...");
        de.run();
        System.out.println("Done!");
    }
*/
}
