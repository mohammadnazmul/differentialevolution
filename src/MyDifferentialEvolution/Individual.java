/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyDifferentialEvolution;

/**
 *
 * @author mohammad
 */
public class Individual {

    private double[] solution;
    private double solnFitness;
    private int geneLength;

    public Individual(int setGeneLength) {
        this.geneLength = setGeneLength;
        this.solution = new double[geneLength];
    }

    public double[] getSolution() {
        return solution;
    }

    public void setSolution(double[] soln, double setFitns) {
        this.solution = soln;
        this.solnFitness = setFitns;
    }

    public double getSolnFitness() {
        return this.solnFitness;
    }

    public String printSolution() {
        String solnStr = "";
        for (int i = 0; i < geneLength; i++) {
            solnStr += (String.format("%.4f", solution[i]) + "\t");
        }
        return solnStr;
    }

}
