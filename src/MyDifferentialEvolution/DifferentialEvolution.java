/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyDifferentialEvolution;

import java.util.Random;

/**
 *
 * @author mohammad
 */
public class DifferentialEvolution {

    private int dimensions;
    private Individual[] population;
    private int populationSize;
    private int maximumIterations;
    private double bestSolutionFitness;
    private Individual bestSolution;
    private double[] allFitness;
    private double xoverProb;
    private double standardDeviation;
    private TheProblem ensembleOfClassifier;
    private double scaleFactor;
    private double[] clsfWeights;

    /**
     * Creates and Executes a new instance of this Differential Evolution
     * implementation. alph = 0.5 xover = 0.9 popSize = 100
     *
     * @param dimension Size of the individuals that will be created
     * @param popSize Size of the population of individuals that will be created
     * @param maxGen Maximum number of Generation that will be performed
     * @param crossoverProb Controls the probability with which an individual
     * will be recombined with the experimental vector created during the
     * mutation phase.
     */
    public DifferentialEvolution(int dimension, int popSize, int maxGen, double standardDeviation, double scaleFactor, double crossoverProb, TheProblem problem, double[] clsWeight) {

        this.dimensions = dimension;
        this.populationSize = popSize;
        this.maximumIterations = maxGen;
        this.xoverProb = crossoverProb;
        this.standardDeviation = standardDeviation;
        this.scaleFactor = scaleFactor;
        this.ensembleOfClassifier = problem;
        population = new Individual[populationSize];
        allFitness = new double[populationSize];
        clsfWeights = clsWeight;
    }

    /**
     * Runs the algorithm until one stop criteria be found.
     */
    public void run() {
        init();
        double currentStandardDeviation;
        int genCntr = 1;
        for (int i = 0; i < maximumIterations; i++) {
            iterate();

            System.out.println("Gen " + genCntr + ": " + String.format("%.4f", bestSolutionFitness) + " ( " + bestSolution.printSolution() + ")");
            currentStandardDeviation = Statistics.getStandardDeviation(allFitness);
            if (currentStandardDeviation < standardDeviation) {
                break;
            }
            genCntr++;
        }
        System.out.println("Solution Found!");
        System.out.println("Generations: " + genCntr);
        System.out.println("Best Individual: " + bestSolution.printSolution());
        System.out.println("Best solution: " + String.format("%.4f", bestSolutionFitness));
    }

    // Initializes the algorithm
    private void init() {
        System.out.println(ensembleOfClassifier.getName());
        System.out.println("Parameters: " + populationSize + "\t" + maximumIterations + "\t" + standardDeviation + "\t" + xoverProb);

        double[] initialSolution;
        for (int i = 0; i < populationSize; i++) {
            population[i] = new Individual(dimensions);
            initialSolution = getInitialSolution();
            population[i].setSolution(initialSolution, ensembleOfClassifier.getFitness(initialSolution));
            allFitness[i] = population[i].getSolnFitness();
        }

        bestSolutionFitness = population[0].getSolnFitness();
        bestSolution = population[0];
        for (Individual individual : population) {
            calculateBestSolution(individual);
        }
    }

    // Performs the iterations of the algorithm
    private void iterate() {
        Individual experimentalIndividual;
        double[] recombinationIndividualSolution;
        double recombinationIndividualSolutionFitness;

        for (int i = 0; i < populationSize; i++) {
            experimentalIndividual = mutation(i);
            recombinationIndividualSolution = crossover(population[i], experimentalIndividual);
            recombinationIndividualSolutionFitness = ensembleOfClassifier.getFitness(recombinationIndividualSolution);

            if (ensembleOfClassifier.compareFitness(population[i].getSolnFitness(), recombinationIndividualSolutionFitness)) {
                population[i].setSolution(recombinationIndividualSolution.clone(),
                        recombinationIndividualSolutionFitness);
                allFitness[i] = recombinationIndividualSolutionFitness;
                calculateBestSolution(population[i]);
            }
        }

    }

    // Performs the mutation phase of the algorithm creating the
    // experimental vector.
    private Individual mutation(int currentIndividualIndex) {
        int targetIndividualIndex = getRandomIndex(currentIndividualIndex, -1, -1);
        int individualAIndex = getRandomIndex(currentIndividualIndex, targetIndividualIndex, -1);
        int individualBIndex = getRandomIndex(currentIndividualIndex, targetIndividualIndex, individualAIndex);

        double[] experimentalSolution = new double[dimensions];
        double[] targetIndividualSolution = population[targetIndividualIndex].getSolution();
        double[] individualASolution = population[individualAIndex].getSolution();
        double[] individualBSolution = population[individualBIndex].getSolution();

        Individual experimentalIndividual = new Individual(dimensions);
        double position;

        for (int i = 0; i < dimensions; i++) {
            position = targetIndividualSolution[i] + scaleFactor * (individualASolution[i] - individualBSolution[i]);
            experimentalSolution[i] = trimPositionToProblemLimits(position, i);
        }
        experimentalIndividual.setSolution(experimentalSolution, ensembleOfClassifier.getFitness(experimentalSolution));

        return experimentalIndividual;
    }

    // Performs the crossover phase of the algorithm, in which occurs the
    // recombination of
    // the current individual with the experimental vector created during the
    // mutation phase.
    private double[] crossover(Individual currentIndividual, Individual experimentalIndividual) {
        Random random = new Random(System.nanoTime());
        double[] recombinationIndividualSolution = currentIndividual.getSolution().clone();
        double[] experimentalIndividualSolution = experimentalIndividual.getSolution();
        double randomValue;

        for (int i = 0; i < dimensions; i++) {
            randomValue = random.nextDouble();
            if (randomValue <= xoverProb) {
                recombinationIndividualSolution[i] = experimentalIndividualSolution[i];
            }
        }

        return recombinationIndividualSolution;
    }

    // Returns an random index between 0 and the number of dimensions of the
    // current problem and that
    // is different from the indexes received as parameters.
    private int getRandomIndex(int currentIndividualIndex, int individualAIndex, int individualBIndex) {
        Random random = new Random(System.nanoTime());
        int randomIndex = random.nextInt(populationSize);

        while (randomIndex == currentIndividualIndex || randomIndex == individualAIndex
                || randomIndex == individualBIndex) {
            randomIndex = random.nextInt(populationSize);
        }

        return randomIndex;
    }

    private void calculateBestSolution(Individual individual) {
        if (ensembleOfClassifier.compareFitness(bestSolutionFitness, individual.getSolnFitness())) {
            bestSolutionFitness = individual.getSolnFitness();
            bestSolution = individual;
        }
    }

    private double trimPositionToProblemLimits(double position, int dimension) {
        double retorno = position;

        if (retorno > ensembleOfClassifier.getUpperLimit(dimension)) {
            retorno = ensembleOfClassifier.getUpperLimit(dimension);
        } else if (retorno < ensembleOfClassifier.getLowerLimit(dimension)) {
            retorno = ensembleOfClassifier.getLowerLimit(dimension);
        }

        return retorno;
    }

    private double[] getInitialSolution() {
        double[] position = new double[dimensions];
        Random random = new Random(System.nanoTime());

        for (int i = 0; i < dimensions; i++) { // Randmoly Select Weights
           /* double gene = random.nextDouble();
            int val = (int) Math.round(gene);
            position[i] = (val * clsfWeights[i]);
*/
            
             double value = random.nextDouble();

             position[i] = (this.ensembleOfClassifier.getUpperLimit(i) - this.ensembleOfClassifier.getLowerLimit(i)) * value + this.ensembleOfClassifier.getLowerLimit(i);
             position[i] = (position[i] <= this.ensembleOfClassifier.getUpperLimit(i)) ? position[i] : this.ensembleOfClassifier.getUpperLimit(i);
             position[i] = (position[i] >= this.ensembleOfClassifier.getLowerLimit(i)) ? position[i] : this.ensembleOfClassifier.getLowerLimit(i);
             
        }

        return position;
    }

}
