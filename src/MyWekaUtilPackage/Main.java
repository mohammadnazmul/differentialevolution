/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyWekaUtilPackage;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohammad
 */
public class Main {
    

    public static void GenerateFullModel(String modelPath, String trnFile, String tstFile, String logFile) {
        try {
            GlobalDataSettings mySettings = new GlobalDataSettings();
            mySettings.enableDebug();
            mySettings.cpuInfo();
            mySettings.setModelPath(modelPath);

            MyWekaUtils myDataManipulator = new MyWekaUtils();
            myDataManipulator.loadTrainData(trnFile);
            myDataManipulator.loadTestData(tstFile);

            MyClassifierUtils modelGen = new MyClassifierUtils();
            modelGen.GenerateAllModelsParallel();
            //modelGen.evaluateAllModels(mySettings.getAllClassifierName(), modelPath, logFile);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void GenerateCVDatasets(String trnFile, String cvDataPath) {
        try {
            GlobalDataSettings mySettings = new GlobalDataSettings();
            mySettings.setSeed(1);
            mySettings.setFold(10);
            mySettings.enableDebug();

            MyWekaUtils myDataManipulator = new MyWekaUtils();
            myDataManipulator.generateCVData(trnFile, cvDataPath);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void GenerateCVModels(String cvDataPath, String cvModelPath) {
        try {
            GlobalDataSettings mySettings = new GlobalDataSettings();
            mySettings.setSeed(1);
            mySettings.setFold(10);

            mySettings.enableDebug();
            mySettings.cpuInfo();
            mySettings.setCVModelPath(cvModelPath);
            mySettings.setCVDataPath(cvDataPath);

            MyWekaUtils myDataManipulator = new MyWekaUtils();
            //myDataManipulator.loadCVTrainData(cvDataPath);
            myDataManipulator.loadAllCVData(cvDataPath);

            MyClassifierUtils modelGen = new MyClassifierUtils();
            modelGen.GenerateCVModelsParallel();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
    public static void main(String[] args) throws Exception {
        //String projPath = "/Users/nazmul/MyGitRepo/Data/";
        String projPath = "/home/mohammad/Dataset/UCI-Datasets/BUPA/";

        String trnFile = projPath + "bupa.arff";
        //String tstFile = projPath + "Test-Data-AD.arff";
        String cvModelPath = projPath + "CVModel/";
        String cvDataPath = projPath + "CVData/";
        //String modelPath = projPath + "FullModel/";
        String logFile = projPath + "Log-BUPA-CVModelPerf.txt";

// Working with datasets        
        //Usage 1:
        // Create CV Datasets
        GenerateCVDatasets(trnFile, cvDataPath);

        // Usage 2:
        // Load Training Data and Generate Full Models
        //GenerateFullModel(modelPath, trnFile, tstFile, logFile);

        // Usage 3:
        // Working with parallel model generators : Genarate CV Models    
        GenerateCVModels(cvDataPath, cvModelPath);

        System.out.println("Done!");
    }
*/

}
