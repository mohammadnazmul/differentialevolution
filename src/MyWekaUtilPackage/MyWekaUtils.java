/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyWekaUtilPackage;

import static MyWekaUtilPackage.GlobalDataSettings.isDebug;
import static MyWekaUtilPackage.GlobalDataSettings.nFolds;
import static MyWekaUtilPackage.GlobalDataSettings.seed;
import static MyWekaUtilPackage.GlobalDataSettings.testDataSet;
import static MyWekaUtilPackage.GlobalDataSettings.trainDataSet;
import static MyWekaUtilPackage.GlobalDataSettings.cvTrnData;
import static MyWekaUtilPackage.GlobalDataSettings.cvTstData;
import static MyWekaUtilPackage.GlobalDataSettings.cvModels;
import static MyWekaUtilPackage.GlobalDataSettings.clsfNames;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 *
 * @author mohammad
 */
public class MyWekaUtils {

    /**
     * Sums the values in the given array.
     *
     * @param values
     * @return the sum of the values in the given array.
     */
    public double ArraySum(double[] values) {
        double sumValue = 0;
        for (double val : values) {
            sumValue += val;
        }
        return sumValue;
    }

    /**
     * Sums the values in the given array.
     *
     * @param values
     * @return the sum of the values in the given array.
     */
    public int ArraySum(int[] values) {
        int sumValue = 0;
        for (int val : values) {
            sumValue += val;
        }
        return sumValue;
    }

    /**
     * Computes the root mean squared error between two vectors.
     *
     * @param a
     * @param b
     * @return the RMSE between a and b
     */
    public double rmse(double[] a, double[] b) {
        if (a.length != b.length) {
            throw new IllegalArgumentException("Arrays must be the same length.");
        }

        double rmse = 0;
        int n = a.length;
        for (int i = 0; i < n; i++) {
            rmse += Math.pow(a[i] - b[i], 2);
        }
        rmse = Math.sqrt(rmse / (double) n);
        return rmse;
    }

    /**
     * Determines whether a data set has equal/balanced class priors.
     *
     * @param data
     * @return whether the data set has equal class priors
     */
    public boolean BalancedClassPriors(Instances data) {
        double[] counts = new double[data.numClasses()];
        int numInstances = data.numInstances();
        for (int i = 0; i < numInstances; i++) {
            Instance inst = data.instance(i);
            int classValueIdx = (int) Math.round(inst.classValue());
            counts[classValueIdx] = counts[classValueIdx] + 1;
        }

        // compute the mean
        double meanCount = ArraySum(counts) / counts.length;
        double[] meanArray = new double[counts.length];
        for (int i = 0; i < meanArray.length; i++) {
            meanArray[i] = meanCount;
        }

        // compute the rmse
        double rmse = rmse(meanArray, counts);

        // compute 2.5% of the possible 
        double deviationAllowed = Math.ceil(0.025 * meanCount);

        if (rmse <= deviationAllowed) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determines whether a data set has equal/balanced class priors.
     *
     * @param data
     * @return whether the data set has equal class priors
     */
    public double[] ClassCounts(Instances data) {
        double[] counts = new double[data.numClasses()];
        int numInstances = data.numInstances();
        for (int i = 0; i < numInstances; i++) {
            Instance inst = data.instance(i);
            int classValueIdx = (int) Math.round(inst.classValue());
            counts[classValueIdx] = counts[classValueIdx] + 1;
        }
        return counts;
    }

    public void loadTrainData(String dsPathName) throws Exception {
        trainDataSet = loadDataset(dsPathName);
    }

    public Instances getTrainData() {
        return trainDataSet;
    }

    public void loadTestData(String dsPathName) throws Exception {
        testDataSet = loadDataset(dsPathName);
    }

    public Instances getTestData() {
        return testDataSet;
    }

    public void loadAllCVData(String cvDataPath) throws Exception {
        cvTrnData = cvDatasetLoader(cvDataPath, nFolds, "train");
        cvTstData = cvDatasetLoader(cvDataPath, nFolds, "test");
    }

    public void loadCVTrainData(String cvDataPath) throws Exception {
        cvTrnData = cvDatasetLoader(cvDataPath, nFolds, "train");
    }

    public void generateCVData(String dsPathName, String cvDataPath) throws Exception {
        // Load and randomize data
        Random rand = new Random(seed);
        Instances data = loadDataset(dsPathName);
        Instances randData = new Instances(data);
        randData.randomize(rand);
        if (randData.classAttribute().isNominal()) {
            randData.stratify(nFolds);
        }

        // Generate Cross validation Folds
        if (isDebug) {
            System.out.println("Start: Generating Cross Validation Folds.");
        }
        for (int n = 0; n < nFolds; n++) {
            Instances train = randData.trainCV(nFolds, n);
            Instances test = randData.testCV(nFolds, n);

            // output "CV Fold" datasets
            ConverterUtils.DataSink.write((cvDataPath + "train-fold-" + n + ".arff"), train);
            if (isDebug) {
                System.out.println("Saved:" + (cvDataPath + "train-fold-" + n + ".arff"));
            }
            ConverterUtils.DataSink.write((cvDataPath + "test-fold-" + n + ".arff"), test);
            if (isDebug) {
                System.out.println("Saved:" + (cvDataPath + "test-fold-" + n + ".arff"));
            }
        }
        if (isDebug) {
            System.out.println("Finish: Generating Cross Validation Folds.");
        }
    }

    public void loadCVModels(String cvModelPath) throws Exception {
        cvModels = cvModelsLoader(clsfNames, cvModelPath, nFolds);
    }

    /* Generic Loader Functions*/
    Instances loadDataset(String dsPathName) throws Exception {
        Instances myData;
        ConverterUtils.DataSource source = new ConverterUtils.DataSource(dsPathName);
        myData = source.getDataSet();
        if (isDebug) {
            System.out.println("Data Set loaded: " + dsPathName);
        }
        myData.setClassIndex(myData.numAttributes() - 1);
        return myData;
    }

    Instances[] cvDatasetLoader(String strCVDataPath, int nfolds, String type) throws Exception {
        Instances[] cvDataFold = new Instances[nfolds];

        if (isDebug) {
            System.out.println("CV data loading Start : " + strCVDataPath);
        }
        for (int i = 0; i < nfolds; i++) {
            String cvFile = strCVDataPath + type + "-fold-" + i + ".arff";
            ConverterUtils.DataSource source = new ConverterUtils.DataSource(cvFile);
            if (isDebug) {
                System.out.println("Loaded: " + cvFile);
            }
            Instances tst = source.getDataSet();
            tst.setClassIndex(tst.numAttributes() - 1);
            cvDataFold[i] = tst;
        }
        if (isDebug) {
            System.out.println("CV data Loading Complete!");
        }

        return cvDataFold;
    }

    Classifier[][] cvModelsLoader(String[] clsfNames, String strCVModelPath, int nfolds) throws Exception {
        Classifier[][] clsCVModels = new Classifier[nfolds][clsfNames.length];

        if (isDebug) {
            System.out.println("Start: Loading Prebuilt Cross Validation Models...\nModels Path: " + strCVModelPath);
        }

        for (int f = 0; f < nfolds; f++) {
            if (isDebug) {
                System.out.print("\nFold " + (f + 1) + ": ");
            }
            for (int ci = 0; ci < clsfNames.length; ci++) {
                String clsfPath = strCVModelPath + clsfNames[ci] + "-" + f + ".model";
                Classifier cls = (Classifier) weka.core.SerializationHelper.read(clsfPath);
                clsCVModels[f][ci] = cls;

                if (isDebug) {
                    System.out.print(clsfNames[ci] + ", ");
                }
            } //END:for nClsf
        }//END: for fold

        if (isDebug) {
            System.out.println("\nFinish: Loading Prebuilt Cross Validation Models...");
        }
        return clsCVModels;
    }

    public void filterFeature(String inpFile, String outFile, String featListFile) {
        try {
            Instances inst;
            Instances instNew;
            Remove remove;
            List<String> attrList = new ArrayList<String>();

            // loads data and set class index
            Instances data = ConverterUtils.DataSource.read(inpFile);
            data.setClassIndex(data.numAttributes() - 1);
            inst = data;
            remove = new Remove();
            for (int ai = 0; ai < inst.numAttributes(); ai++) {
                Attribute nameP = inst.attribute(ai);
                attrList.add(nameP.name());
            }

            if (isDebug) {
                System.out.println("Input Features: " + inst.numAttributes());
            }
            //String featListFile = Utils.getOption("f", args);
            BufferedReader br = new BufferedReader(new FileReader(featListFile));
            String line, featureIndices = "";
            int tot = 0;
            while ((line = br.readLine()) != null) {
                // process the line.
                line = line.trim();
                if (!line.isEmpty()) {
                    int featIndex = attrList.indexOf(line);
                    featIndex++;
                    if (isDebug) {
                        System.out.println(line + ":" + featIndex);
                    }

                    featureIndices += (featIndex + ",");
                    tot++;
                }
            }
            featureIndices += inst.numAttributes();
            br.close();
            if (isDebug) {
                System.out.println(featureIndices + "\nTotal: " + tot);
            }
            remove.setAttributeIndices(featureIndices);
            remove.setInvertSelection(true);
            remove.setInputFormat(inst);
            instNew = Filter.useFilter(inst, remove);
            instNew.setRelationName(inst.relationName());
            ArffSaver saver = new ArffSaver();
            saver.setInstances(instNew);
            saver.setFile(new File(outFile));
            saver.writeBatch();
            if (isDebug) {
                System.out.println("Filtered File Saved in :" + outFile);
            }
        } catch (Exception ex) {
            Logger.getLogger(MyWekaUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
