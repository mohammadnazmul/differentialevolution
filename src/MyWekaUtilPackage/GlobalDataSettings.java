/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyWekaUtilPackage;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.bayes.NaiveBayesUpdateable;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.SGD;
import weka.classifiers.functions.SimpleLogistic;
import weka.classifiers.functions.VotedPerceptron;
import weka.classifiers.lazy.IBk;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.JRip;
import weka.classifiers.rules.OneR;
import weka.classifiers.rules.PART;
import weka.classifiers.rules.ZeroR;
import weka.classifiers.trees.DecisionStump;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.LMT;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.RandomForest;
import weka.classifiers.trees.RandomTree;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class GlobalDataSettings {

    static String trainFile;
    static String testFile;
    static String strModelPath;
    static String strCVModelPath;
    static String strCVDataPath;
    static double[] modelWeight=new double[20];

    static Instances trainDataSet;
    static Instances testDataSet;
    static Instances[] cvTrnData;
    static Instances[] cvTstData;
    public static Classifier[][] cvModels; // fold x cls

    //static Classifier[] singleClassifiers;
    static int nFolds;
    static boolean isDebug;
    static int seed;
    static int processors;

    public static String[] clsfNames = new String[]{"BayesNet",
        "DecisionStump",
        "DecisionTable",
        "IBk",
        "J48",
        "JRip",
        "LibSVM",
        "LMT",
        "Logistic",
        "NaiveBayes",
        "NaiveBayesUpdateable",
        "OneR",
        "PART",
        "RandomForest",
        "RandomTree",
        "REPTree",
        "SGD",
        "SimpleLogistic",
        "VotedPerceptron",
        "ZeroR"
    };

    public static Classifier[] singleClassifiers = {
        new BayesNet(), //Bayes         1
        new DecisionStump(), //Trees    1
        new DecisionTable(), //Rules    1
        new IBk(), // lazy              1
        new J48(), //  1
        new JRip(), //      1
        new LibSVM(), //      1
        new LMT(),
        new Logistic(), // Functions
        new NaiveBayes(), //  1
        new NaiveBayesUpdateable(), //  1
        new OneR(), //      1
        new PART(), //      1
        new RandomForest(), //  1
        new RandomTree(), //  1
        new REPTree(), //  1
        new SGD(),
        new SimpleLogistic(), //      1
        new VotedPerceptron(),
        new ZeroR() //      1
    // Additional Classifiers
    //new MultilayerPerceptron(),
    //new SMO()
    };

    public void setSeed(int seed) {
        this.seed = seed;
    }

    public void setFold(int f) {
        this.nFolds = f;
    }

    public void enableDebug() {
        this.isDebug = true;
    }

    public void disableDebug() {
        this.isDebug = false;
    }

    public void setModelPath(String path) {
        this.strModelPath = path;
    }

    public void setCVModelPath(String path) {
        this.strCVModelPath = path;
    }

    public void setCVDataPath(String path) {
        this.strCVDataPath = path;
    }

    public void setTrainFile(String file) {
        this.trainFile = file;
    }

    public void setTestFile(String file) {
        this.testFile = file;
    }

    public String[] getAllClassifierName() {
        return clsfNames;
    }

    public void cpuInfo() {
        int cpus = Runtime.getRuntime().availableProcessors();
        int useCpu = cpus - 2;
        if (useCpu > singleClassifiers.length) {
            processors = singleClassifiers.length;
        } else {
            processors = useCpu;
        }

        if (isDebug) {
            System.out.println("Available CPUs: " + cpus);
            System.out.println("Using CPUs: " + processors + "\n");
        }
    }

}
