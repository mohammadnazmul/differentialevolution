/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyWekaUtilPackage;

import static MyWekaUtilPackage.GlobalDataSettings.cvTrnData;
import static MyWekaUtilPackage.GlobalDataSettings.cvTstData;
import static MyWekaUtilPackage.GlobalDataSettings.isDebug;
import static MyWekaUtilPackage.GlobalDataSettings.nFolds;
import static MyWekaUtilPackage.GlobalDataSettings.processors;
import static MyWekaUtilPackage.GlobalDataSettings.singleClassifiers;
import static MyWekaUtilPackage.GlobalDataSettings.strCVModelPath;
import static MyWekaUtilPackage.GlobalDataSettings.strModelPath;
import static MyWekaUtilPackage.GlobalDataSettings.trainDataSet;
import static MyWekaUtilPackage.GlobalDataSettings.testDataSet;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class MyClassifierUtils {

    private Classifier fCls;

    
    public  void GenerateFullModel(String modelPath, String trnFile, String tstFile, String logFile) {
        try {
            GlobalDataSettings mySettings = new GlobalDataSettings();
            mySettings.enableDebug();
            mySettings.cpuInfo();
            mySettings.setModelPath(modelPath);

            MyWekaUtils myDataManipulator = new MyWekaUtils();
            myDataManipulator.loadTrainData(trnFile);
            myDataManipulator.loadTestData(tstFile);

            MyClassifierUtils modelGen = new MyClassifierUtils();
            modelGen.GenerateAllModelsParallel();
            //modelGen.evaluateAllModels(mySettings.getAllClassifierName(), modelPath, logFile);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public  void GenerateCVDatasets(String trnFile, String cvDataPath) {
        try {
            GlobalDataSettings mySettings = new GlobalDataSettings();
            mySettings.setSeed(1);
            mySettings.setFold(10);
            mySettings.enableDebug();

            MyWekaUtils myDataManipulator = new MyWekaUtils();
            myDataManipulator.generateCVData(trnFile, cvDataPath);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public  void GenerateCVModels(String cvDataPath, String cvModelPath) {
        try {
            GlobalDataSettings mySettings = new GlobalDataSettings();
            mySettings.setSeed(1);
            mySettings.setFold(10);

            mySettings.enableDebug();
            mySettings.cpuInfo();
            mySettings.setCVModelPath(cvModelPath);
            mySettings.setCVDataPath(cvDataPath);

            MyWekaUtils myDataManipulator = new MyWekaUtils();
            //myDataManipulator.loadCVTrainData(cvDataPath);
            myDataManipulator.loadAllCVData(cvDataPath);

            MyClassifierUtils modelGen = new MyClassifierUtils();
            modelGen.GenerateCVModelsParallel();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // add cls performance logging
     void GenerateAllModelsParallel() throws Exception {
        ParallelModelBuilder fullModelGenPar = new ParallelModelBuilder();
        fullModelGenPar.init(singleClassifiers, trainDataSet);
        fullModelGenPar.setNumExecutionSlots(processors);
        fullModelGenPar.setPath(strModelPath);
        fullModelGenPar.buildClassifierModels();

        // Build all FAILED singleClassifiers serially
        int totFailed = fullModelGenPar.getNumFailed();
        if (totFailed > 0) {
            if (isDebug) {
                System.out.println("Started: " + totFailed + " Failed Model Building");
            }
            ArrayList<String> failedModel = fullModelGenPar.getFailedClassifeirs();
            for (int f = 0; f < totFailed; f++) {
                String currFM = failedModel.get(f);
                BuildFailedModel(currFM);
            } // endFor : totFailed
            if (isDebug) {
                System.out.println("Finished: " + totFailed + "Failed Model Building");
            }
        } // endIF : totFailed
    }

    void BuildFailedModel(String failedModel) throws Exception {
        // Find Classifier from model array
        for (int cl = 0; cl < singleClassifiers.length; cl++) {
            String thisCls = singleClassifiers[cl].getClass().getSimpleName();

            if (thisCls.equals(failedModel)) {
                fCls = singleClassifiers[cl];
                break;
            }
        }

        if (isDebug == true) {
            System.out.println("Start: Building " + fCls.getClass().getSimpleName());
        }
        fCls.buildClassifier(trainDataSet);
        String modPath = strModelPath + failedModel + ".model";

        // serialize model
        weka.core.SerializationHelper.write(modPath, fCls);

        if (isDebug == true) {
            System.out.println("Full Model Saved: " + modPath);
        }
    }

     void GenerateCVModelsParallel() throws Exception {
        ParallelModelBuilder paraModelGenerator = new ParallelModelBuilder();
        paraModelGenerator.init(singleClassifiers, cvTrnData, nFolds);
        paraModelGenerator.setNumExecutionSlots(processors);
        paraModelGenerator.setPath(strCVModelPath);
        paraModelGenerator.buildCVModels();

        // Build all FAILED cv singleClassifiers serially
        int totFailed = paraModelGenerator.getNumFailed();
        if (totFailed > 0) {
            if (isDebug == true) {
                System.out.println("Started: " + totFailed + " Failed CV Model Building");
            }
            ArrayList<String> failedModel = paraModelGenerator.getFailedClassifeirs();
            for (int f = 0; f < totFailed; f++) {
                // Split Classifeir Name & Fold no
                String currFM = failedModel.get(f);
                BuildFailedCVModel(currFM);
            } // endFor : totFailed
            if (isDebug == true) {
                System.out.println("Finished: " + totFailed + " Model Building");
            }
        } // endIF : totFailed
    }

    void BuildFailedCVModel(String failedModel) throws Exception {
        String[] parts = failedModel.split("-");
        String clsName = parts[0];
        int foldNo = Integer.parseInt(parts[1]);

        // Find Classifier from model array
        for (int cl = 0; cl < singleClassifiers.length; cl++) {
            String thisCls = singleClassifiers[cl].getClass().getSimpleName();
            if (thisCls.equals(clsName)) {
                fCls = singleClassifiers[cl];
                break;
            }
        }

        if (isDebug == true) {
            System.out.println("Start: Building " + fCls.getClass().getSimpleName() + "-" + foldNo);
        }
        Instances train = cvTrnData[foldNo];
        fCls.buildClassifier(train);

        // serialize model
        String saveName = strModelPath + failedModel + ".model";
        weka.core.SerializationHelper.write(saveName, fCls);

        if (isDebug == true) {
            System.out.println("CV Model Saved: " + saveName);
        }
    }

    public void evaluateAllModels(String[] clsfNames, String modelPath, String logFile) {
        BufferedWriter writer = null;
        try {
            if (isDebug) {
                System.out.println("Start: Evaluating Prebuilt Full Models...\nModels Path: " + modelPath);
            }
            String result = "Classifier \tTP \tTN \tFP \tFN \tMCC \tAccuracy \tFMeasure \tPrecision\n";
            for (int ci = 0; ci < clsfNames.length; ci++) {
                try {
                    String clsfPath = modelPath + clsfNames[ci] + ".model";
                    Classifier cls = (Classifier) weka.core.SerializationHelper.read(clsfPath);
                    // evaluate classifier and print some statistics
                    Evaluation eval = new Evaluation(trainDataSet);
                    eval.evaluateModel(cls, testDataSet);

                    String resLine = clsfNames[ci];
                    resLine += ("\t" + eval.numTruePositives(0));
                    resLine += ("\t" + eval.numTrueNegatives(0));
                    resLine += ("\t" + eval.numFalsePositives(0));
                    resLine += ("\t" + eval.numFalseNegatives(0));

                    resLine += ("\t" + eval.weightedMatthewsCorrelation()); //MCC
                    resLine += ("\t" + eval.pctCorrect());                  // Accuracy
                    resLine += ("\t" + eval.fMeasure(0));                   // FMeasure
                    resLine += ("\t" + eval.precision(0));                  // Precision
                    resLine += "\n";

                    result += resLine;
                } //END:for nClsf
                catch (Exception ex) {
                    Logger.getLogger(MyClassifierUtils.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(result);
            //Close writer
            writer.close();
            if (isDebug) {
                System.out.println("\nFinish: Evaluating Prebuilt Full Models...");
            }
        } catch (IOException ex) {
            Logger.getLogger(MyClassifierUtils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(MyClassifierUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
