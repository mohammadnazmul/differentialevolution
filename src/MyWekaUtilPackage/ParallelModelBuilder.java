package MyWekaUtilPackage;

import static MyWekaUtilPackage.GlobalDataSettings.cvTstData;
import static MyWekaUtilPackage.GlobalDataSettings.modelWeight;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class ParallelModelBuilder {

    protected boolean isDebug = true;
    protected Classifier[] models;
    protected Instances[] cvTrnData;
    protected Instances trnData;
    protected int numFolds;
    protected String savePath;

    /**
     * The number of threads to have executing at any one time
     */
    protected int m_numExecutionSlots = 8;
    /**
     * Pool of threads to train models with
     */
    protected transient ThreadPoolExecutor m_executorPool;
    /**
     * The number of classifiers completed so far
     */
    protected int m_completed;
    /**
     * The number of classifiers that experienced a failure of some sort during
     * construction
     */
    protected int m_failed;
    protected ArrayList<String> failed = new ArrayList<String>();

    public void enableDebug() {
        isDebug = true;
    }

    public void disableDebug() {
        isDebug = false;
    }

    public void init(Classifier[] models, Instances[] trnCVData, int nfolds) { // Instances[] tstCVData,
        this.models = models;
        this.cvTrnData = trnCVData;
        //this.cvTstData = tstCVData;
        this.numFolds = nfolds;
    }

    public void init(Classifier[] models, Instances trnData) { // Instances[] tstCVData,
        this.models = models;
        this.trnData = trnData;
    }

    public void setNumExecutionSlots(int numSlots) {
        m_numExecutionSlots = numSlots;
    }

    public void setPath(String path) {
        this.savePath = path;
    }

    /**
     * Get the number of execution slots (threads) to use for building the
     * members of the ensemble.
     *
     * @return the number of slots to use
     */
    public int getNumExecutionSlots() {
        return m_numExecutionSlots;
    }

    public int getNumFailed() {
        return m_failed;
    }

    public ArrayList<String> getFailedClassifeirs() {
        return failed;
    }

    /**
     * Start the pool of execution threads
     */
    protected void startExecutorPool() {
        if (m_executorPool != null) {
            m_executorPool.shutdownNow();
        }

        m_executorPool = new ThreadPoolExecutor(m_numExecutionSlots, m_numExecutionSlots,
                120, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
    }

    private synchronized void block(boolean tf) {
        if (tf) {
            try {
                if (m_numExecutionSlots > 1 && m_completed + m_failed < models.length) {
                    wait();
                }
            } catch (InterruptedException ex) {
            }
        } else {
            notifyAll();
        }
    }

    /**
     * Does the actual construction of models for the classifiers
     *
     * @throws Exception if something goes wrong during the training process
     */
    protected synchronized void buildClassifierModels() throws Exception {
        if (isDebug == true) {
            System.out.println("Classifier Model Build: Starting executor pool with " + m_numExecutionSlots
                    + " slots...");
        }
        startExecutorPool();
        m_completed = 0;
        m_failed = 0;

        for (int i = 0; i < models.length; i++) {
            if (m_numExecutionSlots > 1) {

                final Classifier currentClassifier = models[i];
                final String clsName = currentClassifier.getClass().getSimpleName();
                final int iteration = i;
                final String modelname = (savePath + clsName + ".model");
                File f = new File(modelname);

                if (f.exists()) {
                    System.out.println("Model Already Exist: " + clsName);
                    m_completed++;
                } else {

                    Runnable newTask = new Runnable() {
                        public void run() {
                            try {
                                currentClassifier.buildClassifier(trnData);
                                // serialize model
                                //String modelName = (savePath + clsName + ".model");
                                weka.core.SerializationHelper.write(modelname, currentClassifier);

                                if (isDebug == true) {
                                    System.out.println("Saved classifier (" + (iteration + 1) + "): " + modelname);
                                }

                                //completedClassifier(iteration, true);
                                completedClassifier(clsName, true);

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                //completedClassifier(iteration, false);
                                completedClassifier(clsName, false);
                            }
                        }
                    };
                    m_executorPool.execute(newTask);
                }

            } else {
                models[i].buildClassifier(trnData);
            }
        }// end: for

        if (m_numExecutionSlots > 1 && m_completed + m_failed < models.length) {
            block(true);
        }
    }

    /**
     * Does the actual construction of the CV Models
     *
     * @throws Exception if something goes wrong during the training process
     */
    protected synchronized void buildCVModels() throws Exception {
        if (isDebug == true) {
            System.out.println("CV Model Build: Starting executor pool with " + m_numExecutionSlots
                    + " slots...");
        }
        startExecutorPool();
        m_completed = 0;
        m_failed = 0;

        for (int n = 0; n < numFolds; n++) {
            final Instances train = cvTrnData[n];
            final Instances test = cvTstData[n];

            for (int i = 0; i < models.length; i++) {
                if (m_numExecutionSlots > 1) {

                    final Classifier currentClassifier = models[i];
                    final int iteration = i;
                    final int fold = n;
                    final String clsName = (currentClassifier.getClass().getSimpleName() + "-" + fold);
                    final String modelname = (savePath + clsName + ".model");
                    File f = new File(modelname);
                    if (f.exists()) {
                        System.out.println("Model Already Exist: " + clsName);
                        m_completed++;
                    } else {

                        Runnable newTask = new Runnable() {
                            public void run() {
                                try {
                                    currentClassifier.buildClassifier(train);
                                    // evaluate classifier and print some statistics
                                    //Evaluation eval = new Evaluation(train);
                                    //eval.evaluateModel(currentClassifier, test);
                                    //modelWeight[iteration] += eval.weightedMatthewsCorrelation();

                                    // serialize model
                                    weka.core.SerializationHelper.write(modelname, currentClassifier);

                                    if (isDebug == true) {
                                        System.out.println("Saved classifier (" + (iteration + 1) + "):" + modelname);
                                    }

                                    completedClassifier(clsName, true);
                                    //completedClassifier(iteration, true);

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    //completedClassifier(iteration, false);
                                    completedClassifier(clsName, false);
                                }
                            }
                        };

                        // launch this task
                        m_executorPool.execute(newTask);
                    }
                } else {
                    models[i].buildClassifier(train);
                }
            }
        }

        if (m_numExecutionSlots > 1 && m_completed + m_failed < models.length) {
            block(true);
        }
    }

    /**
     * Records the completion of the training of a single classifier. Unblocks
     * if all classifiers have been trained.
     *
     * @param iteration the iteration that has completed
     * @param success whether the classifier trained successfully
     */
    protected synchronized void completedClassifier(String cls, boolean success) {

        if (!success) {
            m_failed++;
            failed.add(cls);

            if (isDebug) {
                System.err.println("Failed Model of Classifier: " + cls);
            }
        } else {
            m_completed++;
        }

        if (m_completed + m_failed == models.length) {
            //  try {
            if (m_failed > 0) {
                if (isDebug) {
                    System.err.println("Problem building classifiers - " + m_failed + " Models building failed.");
                }
            }

                // have to shut the pool down or program executes as a server
            // and when running from the command line does not return to the
            // prompt
            m_executorPool.shutdown();
            block(false);
                // Disable new tasks from being submitted
        /*        
             // Wait for everything to finish.
             while (!m_executorPool.awaitTermination(10, TimeUnit.SECONDS)) {
             if (isDebug == true) {
             System.out.println("Awaiting completion of threads.");
             }
             }
                
             } catch (InterruptedException ex) {
             Logger.getLogger(ParallelModelBuilder.class.getName()).log(Level.SEVERE, null, ex);
             }
             */
        }
    }
}
